﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CapsuleMovement : MonoBehaviour {

    public float moveSpeed;
    public Rigidbody rigidBody;

    public Vector3 moveInput;
    public Vector3 moveVelocity;

    private Camera mainCamera;

    public GunController theGun;
    // Use this for initialization
    void Start () {

        rigidBody = GetComponent<Rigidbody>();
        mainCamera = FindObjectOfType<Camera>();
    }

    
    void Update()
    {

        moveInput = new Vector3(Input.GetAxisRaw("Horizontal"), 0f, Input.GetAxisRaw("Vertical"));
        moveVelocity = moveInput * moveSpeed;

        Ray cameraRay = mainCamera.ScreenPointToRay(Input.mousePosition);
        Plane groundPlane = new Plane(Vector3.up, Vector3.zero);
        float rayLength;

        if(groundPlane.Raycast(cameraRay, out rayLength))
        {
            Vector3 pointToLook = cameraRay.GetPoint(rayLength);
            Debug.DrawLine(cameraRay.origin, pointToLook, Color.blue);

            transform.LookAt(pointToLook);
        }
        if(Input.GetMouseButtonDown(0))
        {
            theGun.fire = true;
        }
        if(Input.GetMouseButtonUp(0)) { theGun.fire = false; }
    }

     void FixedUpdate() { rigidBody.velocity = moveVelocity; }
}

