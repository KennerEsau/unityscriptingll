﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnerMovement : MonoBehaviour {

    //public Transform PlayerTransform, SpawnTrasnform;
    public GameObject Spawn;

    void Start()
    {
        /*SpawnTrasnform = this.transform;
            var player = Instantiate(PlayerTransform, SpawnTrasnform.position, SpawnTrasnform.rotation) as Transform;
                player.SendMessage("Ready",true);*/
    
}
    // Update is called once per frame
    void Update () {
        if (Input.GetKeyDown(KeyCode.Z))
        {

            GameObject objAux = Instantiate(Spawn, transform.position, Quaternion.identity) as GameObject;
            float scale = Random.Range(1, 10);
            objAux.transform.localScale = new Vector3(scale, scale, scale);
            Rigidbody rb = objAux.GetComponent<Rigidbody>();
            rb.drag = Random.Range(0f, 0.1f);
            rb.mass = Random.Range(1, 25);
            rb.AddForce(Vector3.right * Random.Range(10, 150), ForceMode.Impulse);
        }
    }
}
