﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GunController : MonoBehaviour {

    public bool fire;
    public Bala Bala;
    public float balaSpeed;

    public float timeBetweenShots;
    private float shotCounter;

    public Transform firePoint;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
        if(fire)
        {
            shotCounter -= Time.deltaTime;
            if(shotCounter <= 0)
            {
                shotCounter = timeBetweenShots;
            }
            Bala newBala = Instantiate(Bala, firePoint.position, firePoint.rotation) as Bala;

            newBala.speed = balaSpeed;

        }
        else { shotCounter = 0; }
	}
}
